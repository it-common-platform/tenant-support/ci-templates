# GitLab CI Templates

This repository contains various pipeline templates that can be used within your own pipelines. All pipeline definitions here are supported when using runners on the Common Application Platform clusters.

## Repo Structure

This repo contains two different types of templates, depending on what you need. Think of the first collection as job templates, while the second as pipeline templates.

- **/building-blocks/** - these templates provide useful snippets that you can use to build your own templates. They provide only anchors, allowing you to mix and match or reuse each anchor as you need.
- **/pipelines/** - these are fully ready-to-go templates that you can simply extend and use. They provide an entire pipeline for common use patterns, leveraging the building block templates. Be sure to open the template for documentation on any pre-configuration that might be needed for the template to work. 


## Using the Pipeline Templates

The pipeline templates are opinionated and complete pipelines based on common patterns and use cases. For the most part, you will only need to extend a single manifest and get the full pipeline. Be sure to read the template, as each will have setup requirements.

To use a pipeline template, create a `.gitlab-ci.yml` file at the root of your repository and include the template. An example might look like this:

```yaml
include:
  project: it-common-platform/tenant-support/ci-templates
  file: pipelines/docker-build-and-manifest-repo-update.yml
```

Recognizing that these pipelines are very opinionated and may not be as flexible as you need, you can use the building blocks to easily create your own pipeline, but leverage common patterns and best practices.


## Using the Building Blocks

These templates provide only anchors (portions of configuration prefixed with a `.`, such as `.docker-build`), allowing you to reuse each piece as many times as necessary (eg, multiple Docker builds you need to run in a single repo). As such, you will still need to define a small amount of pipeline configuration in your own repository.

1. First, identify the template you want to leverage.
1. In your own repo, create a `.gitlab-ci.yml` if you don't have one.
1. Add an `include` keyword to pull in the template

    ```yaml
    include:
      project: it-common-platform/tenant-support/ci-templates
      file: building-blocks/the-file-to-use.yml
    ```

1. Define your pipeline jobs and extend the desired steps from the template.

    ```
    Build image:
      stage: build
      extends: .docker-build
    ```

1. Commit and go!


### Example: Performing a simple Docker build

This example uses the Docker template to perform a rootless Docker build on the platform cluster.

```yaml
include:
  project: it-common-platform/tenant-support/ci-templates
  file: building-blocks/docker-build.yml

stages:
  - build

Build image:
  stage: build
  extends: .docker-build
```


### Performing Custom Setup/Teardown

For the most part, all of the templates leverage only the `script` portion of the pipeline definition. If you need to perform any custom setup or teardown, you can use `before_script` and/or `after_script` to add your own scripts.

```yaml
# Example to go here once usage is defined
```

### Overriding Template Variables

Every template contains many variables that can be overridden and are documented within the template itself. Due to GitLab's [CI/CD variable precedence](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence), you can simply override the variables you need.

#### Using project-level variables

Leveraging [CI/CD variables on a project](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project), you can define a variable with the same name and override it. This would be a good idea when you need to set a secret that you don't want directly in your `.gitlab-ci.yml` file (such as overriding the `REGISTRY_PASSWORD` in the Docker template).


## Writing Template Guidelines

Contributing to existing templates or adding new templates is welcome and encouraged. Keep in mind the following principles when developing writing templates.

- **Properly namespace any variables.** Prefix all template variables to help reduce conflict with [GitLab CI predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) and any other variables users might use.
- **Give custom variables sane default values, but allow overrides.** Wherever possible, provide opportunities to change behavior. Examples might be extra arguments to pass into CLI execution and change of container registry location.
- **Use only `script` for the main execution.** This allows others to easily add their own `before_script` or `after_script` for custom setup/cleanup.
- **Detect missing required parameters and provide helpful messages.** Be helpful and don't fail without providing the user the ability to debug/troubleshoot. For example, to use GitLab's container registry, it must be enabled in the repo settings. If the setting isn't enabled and the template is trying to push there, notify the user and give them instructions on how to resolve the issue.
- **Use non-zero exit codes on failures or invalid state.** If a job can't complete successfully, make sure a non-zero exit code is returned to ensure the user's pipeline doesn't continue assuming the job succeeded.
